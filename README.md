# Documentação Backend Felipe

## API Routes

**OBS: Para os GETs, usar parâmetro em query, por exemplo, /getalgumacoisa?id=0**


| Verbo  | Rota                      | Descrição                           | Request Body | Response Body | Obs |
| ------ | ------------------------- | ----------------------------------- | ------------ | ------------- | ------ |  
| `POST`   | /api/v1/login                | Authetica usuário         | [Login](#Login) | [LoginResponse](#LoginResponse) |        |
| `POST`    | /api/v1/esenha                     | Formulário para reset de senha        | [EsqueciSenha](#EsqueciSenha) | [LoginResponse](#LoginResponse) |        |
| `POST`    | /api/v1/asenha                     | Altera a senha        | [NovaSenha](#NovaSenha) | [LoginResponse](#LoginResponse) |        |
| `POST`    | /api/v1/addstage                    | Adiciona Fase | [AddStage](#AddStage) | [LoginResponse](#LoginResponse) |  |
| `POST`   | /api/v1/editinfo                    | Editar informações da Trilha                | [EditarInfo](#EditarInfo) | [LoginResponse](#LoginResponse) |        |
| `POST`    | /api/v1/criatrilha            | Cria uma Trilha        | [CriarTrilha](#CriarTrilha) | [LoginResponse](#LoginResponse) |        |
| `POST` | /api/v1/adduser            | Adiciona um usuário ao curso em andamento                     | [AddUserTrilha](#AddUserTrilha) | - |        |
| `POST`    | /api/v1/edituser | Editar perfil de usuário       |  [EditUser](#EditUser) | [LoginResponse](#LoginResponse)       | - |
| `POST`   | /api/v1/addcardgeneric | Adiciona card (vale para três tipos)      | [AddCardTipoInformacaoSubjetivaUpload](#AddCardTipoInformacaoSubjetivaUpload) | [LoginResponse](#LoginResponse) |        |
| `POST`    | /api/v1/addcardobj  | Adiciona card de pergunta objetiva | [AddCardTipoObjetiva](#AddCardTipoObjetiva) | [LoginResponse](#LoginResponse) | - |
| `POST`    | /api/v1/cardanswers | Envia respostas dos cards para o Backend | [MandarRespostas](#MandarRespostas) | [LoginResponse](#LoginResponse)  | - |
| `GET`    | /api/v1/gethome | Fornece informações a serem oferecidas na view de Home | - | [RenderHome](#RenderHome)  | Passar o ID do usuário logado, mas talvez nem precise, pois o backend suporta session... |
| `GET`    | /api/v1/getprofile | Fornece informações a serem oferecidas na view de Profile | - | [ShowProfile](#ShowProfile)  | Passar o ID do usuário que será visualizado |
| `GET`    | /api/v1/getcurso | Fornece informações a serem oferecidas sobre um dado curso | - | [ShowCurso](#ShowCurso)  | Passar o ID do curso |
| `GET`    | /api/v1/getuser | Fornece informações a serem oferecidas sobre um dado usuário | - | [ShowUser](#ShowUser)  | Passar o ID do usuário |
| `GET`    | /api/v1/getmembers | Fornece informações a serem oferecidas na view de membros | - | [ShowMembers](#ShowMembers)  | Passar o ID do curso |
| `GET`    | /api/v1/getcards | Fornece informações a serem oferecidas na view onde serão exibidos os cards | - | [ShowCards](#ShowCards)  | Passar o ID do card como parâmetro GET |
| `GET`    | /api/v1/favoritacard | Favorita um card para o usuário logado | - | - | Passar o ID do card como parâmetro GET |
## Request Body (Do frontend para o Backend)

### Login  
```ts
type Login {
  "usuario": string,
  "senha": string
}
```

### EsqueciSenha  
```ts
type EsqueciSenha 
{
  "email": string
}
```

### NovaSenha  
```ts
type NovaSenha 
{
  "novasenha1": string,
  "novasenha2": string
}
```

### AddStage  
```ts
type AddStage {
  "titulo": string,
  "horas": number,
  "pontos": number,
  "numero_de_cards": number
}

```

### EditarInfo  
```ts
type EditarTrilha {
  "titulo": string,
  "horas": number,
  "descricao": string
}
```

### CriarTrilha  
```ts
type CriarTrilha {
  "titulo": string,
  "horas": number,
  "descricao": string
}
```

### AddUserTrilha  
```ts
type AddUserTrilha {
  "idcurso": number
  "email": string
}
```

### EditUser
```ts
type EditUser {
  "nome": string,
  "email": string,
  "data_nasc": string,
  "username": string
}
```

### AddCardTipoInformacaoSubjetivaUpload
```ts
type AddCardTipoInformacaoESubjetiva {
      "tipo": "informacao" | "psubj" | "upload",
      "titulo": string,
      "corpo": string
}
```

### AddCardTipoObjetiva
```ts
type AddCardTipoObjetiva {
      "tipo": "pobj",
      "titulo": string,
      "numalt": number,
      "corpo": [string,string,string,...]
      "respcorretas": [number,number,...]
}
```



### MandarRespostas  
```ts
type MandarRespostas {
  "numero_de_cards": number,
  "cards": [{"id" : number,
      "tipo": "psubj",
      "resposta": string
    },{"id" : number,
      "tipo": "pobj",
      "resposta": [number,number,...],
    },{"id" : number,
      "tipo": "upload",
      "file": string
    }
  ]
}
```

## Response Body  (do Backend para o Frontend)

### LoginResponse  
```ts
type LoginResponse {
  "sucesso": boolean
}
```

### RenderHome  
```ts
type RenderHome {
  "nickname_usuario": string,
  "id_user":number,
  "cargo": string,
  "numero_de_horas": number,
  "pontuacao_geral": number,
  "cursos": {
    "id_curso": number,
    "numero_de_cursos": number,
    "listacursos":[{
      "titulo_do_curso": string,
      "empresa": string,
      "numero_de_horas": number,
      "desempenho_usuario": {
        "pct_completado": number,
        "numero_de_pontos": number
      }
    },{
      "id_curso": number,
      "titulo_do_curso": string,
      "empresa": string,
      "numero_de_horas": number,
      "desempenho_usuario": {
        "pct_completado": number,
        "numero_de_pontos": number
      }
      ]
    }
  }
}
```

### ShowProfile  
```ts
type ShowProfile {
  "nickname_usuario": string,
  "id_user":number,
  "cargo": string,
  "numero_de_horas": number,
  "pontuacao_geral": number
}
```

### ShowCurso
```ts
type ShowCurso {
  "curso_id":number,
  "descricao":string,
  "horas":number,
  "pontos":number
}
```



### ShowUser
```ts
type ShowUser {
  "nome": string,
  "email": string,
  "data_nasc": string,
  "username": string
}
```

### ShowMembers  
```ts
type ShowMembers {
  "curso_id": number,
  "participantes": {
    "numero_participantes": number,
    "listaparticipantes": [{
      "nickname": string,
      "pontuacao": number
    },{
      "nickname": string,
      "pontuacao": number
    }]
  }
}
```

### ShowCards  
```ts
type ShowCards {
  "numero_de_cards": number,
  "cards": [{
      "id" : number,
      "tipo": "informacao" | "psubj" | "upload",
      "titulo": string,
      "corpo": string
      "favoritado":boolean
    },{"id" : number,
      "tipo": "informacao" | "psubj" | "upload",
      "titulo": string,
      "corpo": string,
      "favoritado":boolean
    },{"id" : number,
      "tipo": "pobj",
      "titulo": string,
      "numalt": number,
      "corpo": {
        "alternativa1": string,
        "alternativa2": string,
        "alternativa3": string
      },
      "favoritado":boolean
    }
  ]
}
```

